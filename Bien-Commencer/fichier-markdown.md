# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien
*Une phrase en italique*
**Une phrase en gras**
Un lien vers [fun-mooc.fr](https://lms.fun-mooc.fr/)
Une ligne de ``` code

## Sous-partie 2 : listes

### Liste à puce

* item
 * sous-item
 * sous-item
* item
* item

### Liste numérotée

1. item 1
2. item 2
3. item 3

## Sous-partie 3 : code


``` # Extrait de code